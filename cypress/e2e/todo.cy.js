describe("Actions sur les tâches", ()=> {
    beforeEach(() => {
        cy.visit("http://localhost:3000/")
        cy.get("button").click()
    })

    it("Ajout d'une todo et affichage",() => {
        cy.get("#title").type("Task 1")
        cy.get("button").click()
    })
    describe("Ajout au préalable de deux tâches", () => {
        beforeEach(() => {
            cy.get("#title").type("Task 1")
            cy.get("button").click()
            cy.get("#title").type("Task 2")
            cy.get("button").click()
        })
        it("Compteur du nombre de todos", () => {
            cy.get('#container > :nth-child(4)').contains("2")
        })
        it("Sélection d'une todo et actualisation automatique du compteur de todos sélectionnées", () => {
            cy.wait(2000)
            cy.get("[type='checkbox']").check()
            cy.contains("Selected Todos: 2")
        })
    })

})